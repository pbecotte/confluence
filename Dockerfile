FROM java

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
    tar \
    wget \
    && apt-get autoremove \
    && apt-get clean

ADD response.varfile /

RUN mkdir /opt/confluence \
 && cd /opt/confluence \
 && wget -q http://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-5.5.3-x64.bin\
 && chmod +x atlassian-confluence-5.5.3-x64.bin \
 && ./atlassian-confluence-5.5.3-x64.bin -q -varfile /response.varfile

RUN mkdir -p /opt/confluence/confluence/WEB-INF/classes/ \
 && echo "confluence.home=/data" > opt/confluence/confluence/WEB-INF/classes/confluence-init.properties

VOLUME /data

EXPOSE 8090

CMD ["/opt/atlassian/confluence/bin/start-confluence.sh", "-fg"]

